/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN
#include "catch2/catch.hpp"
#include "digilent/waveforms/dwf.h"
#include "eeprom_i2c.h"
#include "i2c.h"
#include <chrono>
#include <cstddef>
#include <thread>

using namespace drivers;

// Hardware setup
// Analog Discovery 2 connected to a 24AA01T EEPROM.

void sleep(int milliseconds) {
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
}

class I2C_impl : public drivers::I2C {
public:
    void open() {
        // Open Analog Discovery 2
        FDwfDeviceCloseAll();
        sleep(disconnect_delay);
        int status = FDwfDeviceOpen(first_device, &handle);

        if (status == status_error) {
            throw std::runtime_error("Unable to open the device");
        }

        // Set power to 3.3V
        FDwfAnalogIOReset(handle);
        FDwfAnalogIOChannelNodeSet(handle, positive_supply_channel, positive_supply_node_voltage, v_supply);
        FDwfAnalogIOChannelNodeSet(handle, positive_supply_channel, positive_supply_node_enable, set);
        FDwfAnalogIOEnableSet(handle, set);
        sleep(power_up_delay);

        // Open I2C
        FDwfDigitalI2cReset(handle);
        FDwfDigitalI2cRateSet(handle, i2c_rate_hz);
        FDwfDigitalI2cSclSet(handle, i2c_scl_pin);
        FDwfDigitalI2cSdaSet(handle, i2c_sda_pin);
    }

    // dev_address is the 7 bit address of the device.
    void read(std::uint8_t dev_address, std::byte* begin, const std::byte* end, Error& error) override {
        error            = Error::ok;
        std::size_t size = end - begin;
        int         nak;

        std::uint8_t address = (dev_address << 1) | 1;
        FDwfDigitalI2cRead(handle, address, (unsigned char*)begin, size, &nak);

        if (nak > 0) {
            error = Error::error;
        }
    }

    // dev_address is the 7 bit address of the device.
    void write(std::uint8_t dev_address, const std::byte* begin, const std::byte* end, Error& error) override {
        error            = Error::ok;
        std::size_t size = end - begin;
        int         nak;

        std::uint8_t address = dev_address << 1;
        FDwfDigitalI2cWrite(handle, address, (unsigned char*)begin, size, &nak);

        if (nak > 0) {
            error = Error::error;
        }
    }

    void close() {
        FDwfAnalogIOEnableSet(handle, clear);
        FDwfDeviceClose(handle);
    }

private:
    static constexpr int    first_device                 = -1;
    static constexpr int    status_error                 = 0;
    static constexpr int    positive_supply_channel      = 0;
    static constexpr int    positive_supply_node_enable  = 0;
    static constexpr int    positive_supply_node_voltage = 1;
    static constexpr int    set                          = 1;
    static constexpr int    clear                        = 0;
    static constexpr int    disconnect_delay             = 100;
    static constexpr int    power_up_delay               = 250;
    static constexpr double v_supply                     = 3.3;
    static constexpr int    i2c_rate_hz                  = 100000;
    static constexpr int    i2c_scl_pin                  = 0;
    static constexpr int    i2c_sda_pin                  = 1;

    HDWF handle;
};

SCENARIO("EEPROM i2c hardware test", "[eeprom_i2c_hardware]") {
    // The test is not using BDD to avoid having to connect / disconnect the device at each step.
    constexpr std::uint8_t page_size = 8;
    constexpr std::uint8_t dev_addr  = 0b1010000;

    I2C_impl io;
    io.open();

    using EEPROM = EEPROM_i2c<page_size>;
    EEPROM_i2c<page_size> eeprom(io, dev_addr);
    EEPROM::Error         error;

    // GIVEN("EEPROM is IDLE")
    REQUIRE(eeprom.is_busy() == false);

    // WHEN("write and read a byte at 0x24")
    static constexpr std::uint8_t word_addr = 0x24;
    static constexpr std::byte    value{0x65};

    eeprom.write(word_addr, value, error);
    while (eeprom.is_busy()) {}
    auto read_value = eeprom.read(word_addr, error);

    // THEN("the read value equals the written value")
    REQUIRE(*read_value == value);
    REQUIRE(error == EEPROM::Error::ok);

    // WHEN("write and read a page at 0x10")
    constexpr std::uint8_t                     page_addr = 0x10;
    constexpr std::array<std::byte, page_size> page_write_value{std::byte{1},
                                                                std::byte{2},
                                                                std::byte{3},
                                                                std::byte{4},
                                                                std::byte{5},
                                                                std::byte{6},
                                                                std::byte{7},
                                                                std::byte{8}};
    std::array<std::byte, page_size>           page_read_value{};

    eeprom.page_write(page_addr, page_write_value.begin(), page_write_value.end(), error);
    while (eeprom.is_busy()) {}
    eeprom.read(page_addr, page_read_value.begin(), page_read_value.cend(), error);

    // THEN("the read value equals the written value")
    REQUIRE(page_read_value == page_write_value);
    REQUIRE(error == EEPROM::Error::ok);

    io.close();
}
// NOLINTEND
