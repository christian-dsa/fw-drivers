/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN
#include "catch2/catch.hpp"
#include "led.h"
#include "mock.h"

using namespace drivers;

class Pwm_mock : public Pwm {
public:
    void init(std::uint32_t period, std::uint32_t pulse_width) {
        pwm_period      = period;
        pwm_pulse_width = pulse_width;
    }

    void open() {
        is_open = true;
    }

    void close() {
        is_open = false;
    }

    std::uint32_t get_period() {
        return pwm_period;
    }

    void set_pulse_width(std::uint32_t width) {
        pwm_pulse_width = width;
    }

    std::uint32_t pwm_period{0};
    std::uint32_t pwm_pulse_width{0};
    bool          is_open{false};
};

SCENARIO("Led", "[Led]") {

    Gpio_mock gpio_mock;

    GIVEN("Led is active low, led is on") {

        Led user_led(gpio_mock, Led::Active::low);
        gpio_mock.current_state = Gpio::State::reset;

        WHEN("turn on the led") {
            user_led.on();

            THEN("led is on, gpio is set low") {
                REQUIRE(gpio_mock.current_state == Gpio::State::reset);
            }
        }

        WHEN("turn off the led") {
            user_led.off();

            THEN("led is off, gpio is set high") {
                REQUIRE(gpio_mock.current_state == Gpio::State::set);
            }
        }

        WHEN("read led status") {
            auto state = user_led.read();

            THEN("led status is returned") {
                REQUIRE(state == Led::State::on);
            }
        }
    }

    GIVEN("Led is active high, led is off") {
        Led user_led(gpio_mock, Led::Active::high);
        gpio_mock.current_state = Gpio::State::reset;

        WHEN("turn on the led") {
            user_led.on();

            THEN("led is on, gpio is set high") {
                REQUIRE(gpio_mock.current_state == Gpio::State::set);
            }
        }

        WHEN("turn off the led") {
            user_led.off();

            THEN("led is off, gpio is set low") {
                REQUIRE(gpio_mock.current_state == Gpio::State::reset);
            }
        }

        WHEN("read led status") {
            auto state = user_led.read();

            THEN("led status is returned") {
                REQUIRE(state == Led::State::off);
            }
        }
    }
}

SCENARIO("Dimmable Led", "[PWM Led]") {  // NOLINT(readability-function-cognitive-complexity)
    static constexpr std::uint16_t pwm_period = (1U << 15U);
    Pwm_mock                       pwm_mock;
    pwm_mock.init(pwm_period, 0);

    GIVEN("Led is controlled by pwm. Led polarity is set via pwm (invert pwm output for active low)") {

        Led_pwm user_led(pwm_mock);

        WHEN("led is opened") {

            user_led.open();

            THEN("led is set to off, pwm is opened") {
                REQUIRE(pwm_mock.is_open == true);
                REQUIRE(pwm_mock.pwm_pulse_width == 0);
            }
        }

        WHEN("turn on the led to 30% using uint8_t") {
            std::uint8_t intensity_uint8 = 76;  // 0.3 * 255

            user_led.on(intensity_uint8);

            THEN("pwm pulse width is set") {
                // Expected pulse width in pwm register
                auto pwm_period_f      = static_cast<float>(pwm_period);
                auto pulse_width_uint8 = static_cast<std::uint32_t>(76.0 / 255.0 * pwm_period_f);

                // Use matching with lambda to check value range
                REQUIRE(pwm_mock.pwm_pulse_width == pulse_width_uint8);
            }
        }

        WHEN("turn on the led to 100% using uint8_t") {
            std::uint8_t intensity_uint8_full = 255;

            user_led.on(intensity_uint8_full);

            THEN("pwm pulse width is set") {
                // Expected pulse width in pwm register
                std::uint32_t pulse_width_full = pwm_period;

                // Use matching with lambda to check value range
                REQUIRE(pwm_mock.pwm_pulse_width == pulse_width_full);
            }
        }

        WHEN("turn on the led to 65.5% using float") {
            float intensity_float = 0.655;

            user_led.on(intensity_float);

            THEN("pwm pulse width is set") {
                // Expected pulse width in pwm register
                auto pwm_period_f      = static_cast<float>(pwm_period);
                auto pulse_width_float = static_cast<std::uint32_t>(intensity_float * pwm_period_f);

                // Use matching with lambda to check value range
                REQUIRE(pwm_mock.pwm_pulse_width == pulse_width_float);
            }
        }

        WHEN("Turn on the led to 100% using uint8_t") {
            std::uint8_t value = 255;
            user_led.on(value);

            THEN("led is set at 100%") {
                REQUIRE(pwm_mock.pwm_pulse_width == pwm_period);
            }
        }

        WHEN("Turn on the led with > 100% using float") {

            user_led.on(330.2);

            THEN("led is set at 100%") {
                static constexpr std::uint32_t pulse_width_full = 1 + pwm_period;
                REQUIRE(pwm_mock.pwm_pulse_width == pulse_width_full);
            }
        }
    }

    GIVEN("The led is on at 50%") {
        float intensity = 0.50;

        Led_pwm user_led(pwm_mock);
        user_led.on(intensity);

        WHEN("the led is turned off") {

            user_led.off();

            THEN("pwm pulse width is set to 0") {
                REQUIRE(pwm_mock.pwm_pulse_width == 0);
            }
        }

        WHEN("led is closed") {

            user_led.close();

            THEN("led is set to off, pwm is closed") {
                REQUIRE(pwm_mock.is_open == false);
            }
        }

        WHEN("the intensity is changed") {
            float new_intensity = 0.13;
            user_led.on(new_intensity);

            THEN("pwm pulse width is set to new intensity") {
                auto pwm_period_f      = static_cast<float>(pwm_period);
                auto pulse_width_float = static_cast<std::uint32_t>(new_intensity * pwm_period_f);
                REQUIRE(pwm_mock.pwm_pulse_width == pulse_width_float);
            }
        }

        WHEN("the led is turned off and back on") {

            user_led.off();
            user_led.on();

            THEN("pwm pulse width is back to the original") {
                auto pwm_period_f      = static_cast<float>(pwm_period);
                auto pulse_width_float = static_cast<std::uint32_t>(intensity * pwm_period_f);
                REQUIRE(pwm_mock.pwm_pulse_width == pulse_width_float);
            }
        }
    }

    GIVEN("Gamma correction") {

        Led_pwm user_led(pwm_mock);

        float gamma = 3.0;

        WHEN("turn on the led") {
            float intensity           = 0.25;
            float intensity_corrected = user_led.gamma_correction(intensity, gamma);
            user_led.on(intensity_corrected);

            THEN("brightness is adjusted") {
                // Final pulse width using gamma = 3.0 for 25% intensity
                static constexpr std::uint32_t pulse_width_gamma = 512;
                REQUIRE(pwm_mock.pwm_pulse_width == pulse_width_gamma);
            }
        }
    }
}
// NOLINTEND
