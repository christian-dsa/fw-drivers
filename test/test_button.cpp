/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN

#include "button.h"
#include "catch2/catch.hpp"
#include "mock.h"

using namespace drivers;

SCENARIO("Button", "[button]") {
    Gpio_mock mock;

    GIVEN("The button is active low, button is not pressed") {
        Button button(mock, Button::Active::low);
        mock.current_state = Gpio_mock::State::set;

        Button::State state = button.read();
        REQUIRE(state == Button::State::idle);

        WHEN("the button is pressed") {
            mock.current_state = Gpio_mock::State::reset;

            state = button.read();

            THEN("read return pressed") {
                REQUIRE(state == Button::State::pressed);
            }
        }

        WHEN("the button is idle") {
            state = button.read();

            THEN("read return pressed") {
                REQUIRE(state == Button::State::idle);
            }
        }
    }

    GIVEN("The button is active low, button is pressed") {
        Button button(mock, Button::Active::low);
        mock.current_state = Gpio_mock::State::reset;

        Button::State state = button.read();
        REQUIRE(state == Button::State::pressed);

        WHEN("the button is release") {
            mock.current_state = Gpio_mock::State::set;
            state              = button.read();

            THEN("read return release") {
                REQUIRE(state == Button::State::released);
            }
        }

        WHEN("the button is still pressed") {
            state = button.read();

            THEN("read return pressed") {
                REQUIRE(state == Button::State::pressed);
            }
        }
    }

    GIVEN("The button is active high, button is not pressed") {
        Button button(mock, Button::Active::high);
        mock.current_state = Gpio_mock::State::reset;

        Button::State state = button.read();
        REQUIRE(state == Button::State::idle);

        WHEN("the button is pressed") {
            mock.current_state = Gpio_mock::State::set;

            state = button.read();

            THEN("read return pressed") {
                REQUIRE(state == Button::State::pressed);
            }
        }

        WHEN("the button is idle") {
            state = button.read();

            THEN("read return pressed") {
                REQUIRE(state == Button::State::idle);
            }
        }
    }

    GIVEN("The button is active high, button is pressed") {
        Button button(mock, Button::Active::high);
        mock.current_state = Gpio_mock::State::set;

        Button::State state = button.read();
        REQUIRE(state == Button::State::pressed);

        WHEN("the button is release") {
            mock.current_state = Gpio_mock::State::reset;
            state              = button.read();

            THEN("read return release") {
                REQUIRE(state == Button::State::released);
            }
        }

        WHEN("the button is still pressed") {
            state = button.read();

            THEN("read return pressed") {
                REQUIRE(state == Button::State::pressed);
            }
        }
    }
}
// NOLINTEND
