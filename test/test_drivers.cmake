find_package(Catch2 REQUIRED)

set(EXECUTABLE test_drivers)
add_executable(${EXECUTABLE} "")

option(HARDWARE_EEPROM_I2C "Enable test with a real eeprom connected to the analog discovery 2" OFF)

target_sources(${EXECUTABLE}
    PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/catch2_main.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_button.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_eeprom_i2c.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_led.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_timer.cpp
    )

target_include_directories(${EXECUTABLE}
    PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}
    )

# Add analog discovery 2 SDK
if (HARDWARE_EEPROM_I2C)
    find_library(LIB_DWF dwf REQUIRED)
else ()
    set(LIB_DWF "")
endif ()

# Hardware compile option
if (HARDWARE_EEPROM_I2C)
    message(STATUS "Enable test with hardware eeprom i2c")
    target_sources(${EXECUTABLE}
        PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test_eeprom_i2c_hw.cpp
        )
endif ()

target_link_libraries(${EXECUTABLE}
    PRIVATE
    ${LIBRARY_NAME}
    Catch2::Catch2
    ${LIB_DWF}
    )

target_compile_features(${EXECUTABLE} PRIVATE cxx_std_17)
