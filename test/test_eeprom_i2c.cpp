/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN
#include "catch2/catch.hpp"
#include "eeprom_i2c.h"
#include <array>
#include <cstddef>

using namespace drivers;

class EEPROM_emulation : public I2C {
public:
    EEPROM_emulation(std::uint8_t device_address, std::size_t page_size)
    : device_address(device_address),
      page_size(page_size) {}

    void init(std::byte register_value_mask) {
        // Set all register value to 0x10 + register address
        std::size_t i = 0;
        for (auto& it : storage) {
            it = register_value_mask | static_cast<std::byte>(i);
            ++i;
        }
    }

    // AT24C01A
    void read(std::uint8_t dev_address, std::byte* begin, const std::byte* end, Error& error) {
        error = Error::ok;

        if (dev_address != device_address) {
            error = Error::error;
            return;
        }

        for (std::byte* it = begin; it < end; ++it) {
            *it = storage.at(address_cursor);
            ++address_cursor;
        }
    }

    void write(std::uint8_t dev_address, const std::byte* begin, const std::byte* end, Error& error) {
        error = Error::ok;

        if ((busy) || (dev_address != device_address)) {
            error = Error::error;
            return;
        }

        address_cursor = static_cast<std::uint8_t>(*begin);

        // Roll over if the input data length is greater than the page size.
        std::uint8_t page_begin = (address_cursor / page_size) * page_size;
        std::size_t  i          = address_cursor % page_size;

        ++begin;
        for (const std::byte* it = begin; it < end; ++it) {
            storage.at(address_cursor) = *it;
            ++i;
            address_cursor = page_begin + (i % page_size);
        }
    }

    void set_busy() {
        busy = true;
    }

    void clear_busy() {
        busy = false;
    }

    static constexpr std::size_t       eeprom_size = 128;  // 128 * 8 = 1k
    std::array<std::byte, eeprom_size> storage{};
    std::size_t                        page_size;
    std::uint8_t                       device_address;
    std::uint8_t                       address_cursor{0};
    bool                               busy{false};
};

SCENARIO("EEPROM i2c", "[eeprom_i2c]") {

    static constexpr std::uint8_t dev_address       = 0b1010000;
    static constexpr std::uint8_t page_size         = 8;
    static constexpr std::byte    eeprom_value_mask = std::byte{0x80};

    EEPROM_emulation      i2c(dev_address, page_size);
    EEPROM_i2c<page_size> eeprom(i2c, dev_address);
    using EEPROM        = EEPROM_i2c<page_size>;
    EEPROM::Error error = EEPROM::Error::ok;

    i2c.init(eeprom_value_mask);

    GIVEN("EEPROM emulator test") {
        EEPROM_emulation::Error i2c_error;

        WHEN("device is busy") {
            std::byte dummy;
            i2c.set_busy();
            i2c.write(dev_address, &dummy, &dummy, i2c_error);

            THEN("it returns error") {
                REQUIRE(i2c_error == EEPROM_emulation::Error::error);
            }
        }

        WHEN("write data and roll over") {
            static constexpr std::array<std::byte, page_size + 4> data = {std::byte{0x02},
                                                                          std::byte{0x10},
                                                                          std::byte{0x11},
                                                                          std::byte{0x12},
                                                                          std::byte{0x13},
                                                                          std::byte{0x14},
                                                                          std::byte{0x15},
                                                                          std::byte{0x16},
                                                                          std::byte{0x17},
                                                                          std::byte{0x18},
                                                                          std::byte{0x19},
                                                                          std::byte{0x1A}};

            i2c.write(dev_address, data.cbegin(), data.cend(), i2c_error);

            THEN("write roll over the page") {
                REQUIRE(i2c_error == EEPROM_emulation::Error::ok);
                REQUIRE(i2c.storage[0] == std::byte{0x16});
                REQUIRE(i2c.storage[1] == std::byte{0x17});
                REQUIRE(i2c.storage[2] == std::byte{0x18});
                REQUIRE(i2c.storage[3] == std::byte{0x19});
                REQUIRE(i2c.storage[4] == std::byte{0x1A});
                REQUIRE(i2c.storage[5] == std::byte{0x13});
                REQUIRE(i2c.storage[6] == std::byte{0x14});
                REQUIRE(i2c.storage[7] == std::byte{0x15});
                REQUIRE(i2c.storage[8] == (eeprom_value_mask | std::byte{8}));
            }
        }

        WHEN("write data in a single register") {
            static constexpr std::array<std::byte, 2> data = {std::byte{0x06}, std::byte{0xF6}};

            i2c.write(dev_address, data.cbegin(), data.cend(), i2c_error);

            THEN("byte is written at desired address") {
                REQUIRE(i2c_error == EEPROM_emulation::Error::ok);
                REQUIRE(i2c.storage[6] == std::byte{0xF6});
            }
        }

        WHEN("read a single register") {
            static constexpr std::array<std::byte, 1> data   = {std::byte{0x06}};
            std::array<std::byte, 1>                  r_data = {};

            i2c.write(dev_address, data.cbegin(), data.cend(), i2c_error);
            i2c.read(dev_address, r_data.begin(), r_data.cend(), i2c_error);

            THEN("byte is read") {
                REQUIRE(i2c_error == EEPROM_emulation::Error::ok);
                REQUIRE(r_data[0] == i2c.storage[6]);
            }
        }

        WHEN("read whole eeprom") {
            std::array<std::byte, EEPROM_emulation::eeprom_size> data;
            data.fill(std::byte{0xFF});

            // Cursor is at 0 when emulator is created.
            i2c.read(dev_address, data.begin(), data.cend(), i2c_error);

            THEN("byte is read") {
                REQUIRE(i2c_error == EEPROM_emulation::Error::ok);
                REQUIRE(data == i2c.storage);
            }
        }
    }

    GIVEN("Device is busy writing data internally") {
        i2c.set_busy();

        WHEN("the device is polled") {
            bool busy = eeprom.is_busy();

            THEN("get signal that device is busy") {
                REQUIRE(busy == true);
            }
        }

        WHEN("read a specific address") {
            auto value = eeprom.read(0x0A, error);

            THEN("error: not_responding") {
                REQUIRE(error == EEPROM::Error::not_responding);
            }
            THEN("no value is received") {
                REQUIRE(value.has_value() == false);
            }
        }

        WHEN("read multiples register") {
            static constexpr std::size_t size = 4;
            std::array<std::byte, size>  read_buffer{};

            eeprom.read(0x0A, read_buffer.begin(), read_buffer.cend(), error);

            THEN("error: not_responding") {
                REQUIRE(error == EEPROM::Error::not_responding);
            }
        }

        WHEN("write multiple byte") {
            static constexpr std::size_t                 size = 4;
            static constexpr std::array<std::byte, size> write_buffer{std::byte{0x01},
                                                                      std::byte{0x02},
                                                                      std::byte{0x03},
                                                                      std::byte{0x04}};

            eeprom.page_write(0x12, write_buffer.cbegin(), write_buffer.cend(), error);

            THEN("error: not_responding") {
                REQUIRE(error == EEPROM::Error::not_responding);
            }
        }

        WHEN("write one byte") {
            static constexpr std::byte value{0xFA};
            eeprom.write(0x12, value, error);

            THEN("error: not_responding") {
                REQUIRE(error == EEPROM::Error::not_responding);
            }
        }
    }

    GIVEN("Device is idle") {
        std::array<std::byte, EEPROM_emulation::eeprom_size> original_state = i2c.storage;

        WHEN("the device is polled") {
            bool busy = eeprom.is_busy();

            THEN("get signal that device is not busy") {
                REQUIRE(busy == false);
            }
        }

        WHEN("write one byte at address 0x23") {
            static constexpr std::uint8_t word_address = 0x23;
            static constexpr std::byte    value{0x14};

            eeprom.write(word_address, value, error);

            THEN("value is written in eeprom") {
                REQUIRE(i2c.storage[0x23] == value);
                REQUIRE(error == EEPROM::Error::ok);
            }
        }

        WHEN("write multiple byte") {
            static constexpr std::uint8_t                      start_address = 0x20;
            static constexpr std::size_t                       write_size    = 3;
            static constexpr std::array<std::byte, write_size> value         = {std::byte{0x12},
                                                                                std::byte{0x34},
                                                                                std::byte{0x56}};

            eeprom.page_write(start_address, value.begin(), value.end(), error);

            THEN("all value are written in eeprom") {
                REQUIRE(i2c.storage[start_address + 0] == value[0]);
                REQUIRE(i2c.storage[start_address + 1] == value[1]);
                REQUIRE(i2c.storage[start_address + 2] == value[2]);
                REQUIRE(error == EEPROM::Error::ok);
            }
        }

        WHEN("write a full page") {
            static constexpr std::uint8_t                     start_address = 0x20;
            static constexpr std::array<std::byte, page_size> value         = {std::byte{0x12},
                                                                               std::byte{0x34},
                                                                               std::byte{0x56},
                                                                               std::byte{0x78},
                                                                               std::byte{0x9A},
                                                                               std::byte{0xBC},
                                                                               std::byte{0xDE},
                                                                               std::byte{0xFF}};

            eeprom.page_write(start_address, value.cbegin(), value.cend(), error);

            THEN("all value are written in eeprom") {
                REQUIRE(i2c.storage[start_address + 0] == value[0]);
                REQUIRE(i2c.storage[start_address + 1] == value[1]);
                REQUIRE(i2c.storage[start_address + 2] == value[2]);
                REQUIRE(i2c.storage[start_address + 3] == value[3]);
                REQUIRE(i2c.storage[start_address + 4] == value[4]);
                REQUIRE(i2c.storage[start_address + 5] == value[5]);
                REQUIRE(i2c.storage[start_address + 6] == value[6]);
                REQUIRE(i2c.storage[start_address + 7] == value[7]);
                REQUIRE(error == EEPROM::Error::ok);
            }
        }

        WHEN("write multiple byte, unaligned") {
            static constexpr std::uint8_t                     start_address = 0x23;
            static constexpr std::array<std::byte, page_size> value         = {std::byte{0x12},
                                                                               std::byte{0x34},
                                                                               std::byte{0x56},
                                                                               std::byte{0x78},
                                                                               std::byte{0x9A},
                                                                               std::byte{0xBC},
                                                                               std::byte{0xDE},
                                                                               std::byte{0xFF}};

            eeprom.page_write(start_address, value.cbegin(), value.cend(), error);

            THEN("error: alignment_error") {
                REQUIRE(error == EEPROM::Error::alignment_error);
                REQUIRE(i2c.storage == original_state);
            }
        }

        WHEN("write multiple byte, size is bigger than a page") {
            static constexpr std::uint8_t                      start_address = 0x20;
            static constexpr std::size_t                       write_size    = 16;
            static constexpr std::array<std::byte, write_size> value         = {std::byte{0x12},
                                                                                std::byte{0x34},
                                                                                std::byte{0x56},
                                                                                std::byte{0x78},
                                                                                std::byte{0x9A},
                                                                                std::byte{0xBC},
                                                                                std::byte{0xDE},
                                                                                std::byte{0xFF},
                                                                                std::byte{0x12},
                                                                                std::byte{0x34},
                                                                                std::byte{0x56},
                                                                                std::byte{0x78},
                                                                                std::byte{0x9A},
                                                                                std::byte{0xBC},
                                                                                std::byte{0xDE},
                                                                                std::byte{0xFF}};

            eeprom.page_write(start_address, value.cbegin(), value.cend(), error);

            THEN("error: size_error") {
                REQUIRE(error == EEPROM::Error::size_error);
                REQUIRE(i2c.storage == original_state);
            }
        }
    }

    GIVEN("Device is idle, register 0x25 has the value 0x91") {
        static constexpr std::uint8_t word_address   = 0x25;
        static constexpr std::byte    expected_value = std::byte{0x91};
        i2c.storage.at(word_address)                 = expected_value;

        WHEN("read a specific address") {

            auto value = eeprom.read(word_address, error);

            THEN("the value is returned") {
                REQUIRE(value == expected_value);
                REQUIRE(error == EEPROM::Error::ok);
            }
        }
    }

    GIVEN("Device is idle, eeprom page is written with an incrementing counter") {
        static constexpr std::uint8_t                     address_start = 0x28;
        static constexpr std::array<std::byte, page_size> expected_data{std::byte{0xA8},
                                                                        std::byte{0xA9},
                                                                        std::byte{0xAA},
                                                                        std::byte{0xAB},
                                                                        std::byte{0xAC},
                                                                        std::byte{0xAD},
                                                                        std::byte{0xAE},
                                                                        std::byte{0xAF}};
        auto*                                             w_it = i2c.storage.begin() + address_start;
        for (auto& it : expected_data) {
            *w_it = it;
            ++w_it;
        }

        WHEN("read a page starting at 0x28") {
            std::array<std::byte, page_size> read_data{};

            eeprom.read(address_start, read_data.data(), read_data.cend(), error);

            THEN("all value are read and received") {
                REQUIRE(read_data == expected_data);
                REQUIRE(error == EEPROM::Error::ok);
            }
        }
    }
}
// NOLINTEND
