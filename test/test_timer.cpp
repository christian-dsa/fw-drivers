/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN

#include "catch2/catch.hpp"
#include "tick.h"
#include "timer.h"
#include <limits>

using namespace drivers;

class Tick_impl : public Tick {
public:
    Tick_impl()
    : tick_value(0) {}

    void start() override {
        tick_value = 0;
    }

    void stop() override {}

    void suspend() override {}

    void resume() override {}

    void inc_tick_value() override {
        ++tick_value;
    }

    std::uint32_t get_tick_value() override {
        ++tick_value;
        return tick_value;
    }

    std::uint32_t get_resolution_ms() override {
        return 1;
    }

    void set_tick_value(std::uint32_t value) {
        tick_value = value;
    }

    std::uint32_t tick_value;
};

SCENARIO("Timer", "[timer]") {

    // Delay function check that value one time before entering the loop
    constexpr std::uint32_t counter_inc_offset = 1;

    Tick_impl tick;  // Tick resolution is 1ms
    Timer     timer(tick);

    GIVEN("The tick timer is running") {
        tick.start();

        WHEN("the application block for 100ms, tick is at 0") {
            constexpr std::uint32_t delay_value = 100;

            tick.set_tick_value(0);
            timer.delay_ms(delay_value);

            THEN("we quit the blocking delay 100ms later") {
                REQUIRE(tick.tick_value == delay_value + counter_inc_offset);
            }
        }

        WHEN("the application block for 100ms, tick is close to overflow") {
            constexpr std::uint32_t start_value = std::numeric_limits<std::uint32_t>::max() - 15;
            tick.set_tick_value(start_value);

            constexpr std::uint32_t delay_value = 100;
            timer.delay_ms(delay_value);

            THEN("we quit the blocking delay 100ms later") {
                REQUIRE(tick.tick_value == start_value + delay_value + counter_inc_offset);
            }
        }
    }

    GIVEN("A 200ms countdown is started") {
        constexpr std::uint32_t tick_start   = 5000;
        constexpr std::uint32_t countdown_ms = 200;

        tick.set_tick_value(tick_start);
        timer.start_countdown(countdown_ms);

        WHEN("50ms is elapsed") {
            constexpr std::uint32_t elapsed_ms = 50;
            tick.set_tick_value(tick_start + elapsed_ms);

            THEN("countdown is not over") {
                REQUIRE(timer.is_countdown_over() == false);
            }
        }

        WHEN("200ms is elapsed") {
            constexpr std::uint32_t elapsed_ms = 200;
            tick.set_tick_value(tick_start + elapsed_ms);

            THEN("countdown is not over") {
                REQUIRE(timer.is_countdown_over() == true);
            }
        }
    }

    GIVEN("A 200ms countdown is started, tick is close to overflow") {
        constexpr std::uint32_t tick_start   = std::numeric_limits<std::uint32_t>::max() - 15;
        constexpr std::uint32_t countdown_ms = 200;

        tick.set_tick_value(tick_start);
        timer.start_countdown(countdown_ms);

        WHEN("50ms is elapsed") {
            constexpr std::uint32_t elapsed_ms = 50;
            tick.set_tick_value(tick_start + elapsed_ms);

            THEN("countdown is not over") {
                REQUIRE(timer.is_countdown_over() == false);
            }
        }

        WHEN("200ms is elapsed") {
            constexpr std::uint32_t elapsed_ms = 200;
            tick.set_tick_value(tick_start + elapsed_ms);

            THEN("countdown is not over") {
                REQUIRE(timer.is_countdown_over() == true);
            }
        }
    }
}
// NOLINTEND
