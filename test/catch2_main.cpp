/*
 * catch2_define.cpp
 *
 *  Created on: Jan. 22, 2020
 *      Author: christian
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#define CATCH_CONFIG_COLOUR_NONE
#include "catch2/catch.hpp"
