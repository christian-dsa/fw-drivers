/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef EEPROM_I2C_H
#define EEPROM_I2C_H

#include "i2c.h"
#include <array>
#include <optional>

namespace drivers {

template<std::size_t page_size>
class EEPROM_i2c {
public:
    enum class Error {
        ok,
        size_error,
        alignment_error,
        not_responding,
    };

    explicit EEPROM_i2c(I2C& i2c, std::uint8_t dev_address = default_address)
    : io(&i2c),
      device_address(dev_address) {}

    // Useful when the page can be selected in the address.
    void set_address(std::uint8_t dev_address) {
        device_address = dev_address;
    }

    /* Return true if eeprom is busy */
    bool is_busy() {
        I2C::Error io_error = I2C::Error::ok;
        io->write(device_address, &no_xfer, &no_xfer, io_error);
        return io_error == I2C::Error::error;
    }

    void write(const std::uint8_t word_address, const std::byte value, Error& error) const {
        I2C::Error io_error = I2C::Error::ok;

        const std::array<std::byte, byte_write_size> buffer{static_cast<std::byte>(word_address), value};

        io->write(device_address, buffer.cbegin(), buffer.cend(), io_error);

        if (io_error == I2C::Error::error) {
            error = Error::not_responding;
        } else {
            error = Error::ok;
        }
    }

    void page_write(const std::uint8_t address, const std::byte* begin, const std::byte* end, Error& error) const {

        I2C::Error        io_error = I2C::Error::ok;
        const std::size_t size     = (end - begin);

        std::array<std::byte, page_size + word_address_byte> page_buffer{};

        if (size > page_size) {
            error = Error::size_error;
            return;
        }

        if ((address % page_size) > (page_size - size)) {
            error = Error::alignment_error;
            return;
        }

        auto w_it = page_buffer.begin();
        *w_it     = static_cast<std::byte>(address);
        ++w_it;

        for (const auto* data_it = begin; data_it != end; ++data_it) {
            *w_it = *data_it;
            ++w_it;
        }

        const std::byte* xfer_end = page_buffer.begin() + size + word_address_byte;
        io->write(device_address, page_buffer.begin(), xfer_end, io_error);

        if (io_error == I2C::Error::error) {
            error = Error::not_responding;
        } else {
            error = Error::ok;
        }
    }

    std::optional<std::byte> read(const std::uint8_t word_address, Error& error) const {
        I2C::Error io_error = I2C::Error::ok;
        std::byte  value{};
        error = Error::ok;

        auto data = static_cast<std::byte>(word_address);
        io->write(device_address, &data, &data + single_byte, io_error);

        if (io_error == I2C::Error::error) {
            error = Error::not_responding;
            return {};
        }

        io->read(device_address, &value, &value + single_byte, io_error);

        if (io_error == I2C::Error::error) {
            error = Error::not_responding;
            return {};
        }

        return value;
    }

    void read(const std::uint8_t address_start, std::byte* begin, const std::byte* end, Error& error) const {
        I2C::Error io_error = I2C::Error::ok;
        error               = Error::ok;

        auto data = static_cast<std::byte>(address_start);
        io->write(device_address, &data, &data + single_byte, io_error);

        if (io_error == I2C::Error::error) {
            error = Error::not_responding;
        }

        io->read(device_address, begin, end, io_error);

        if (io_error == I2C::Error::error) {
            error = Error::not_responding;
        }
    }

private:
    I2C*         io;
    std::uint8_t device_address;

    static constexpr std::uint8_t default_address   = 0b1010000;
    static constexpr std::size_t  single_byte       = 1;
    static constexpr std::size_t  word_address_byte = 1;
    static constexpr std::size_t  byte_write_size   = 2;
    static constexpr std::byte    no_xfer{0};
};
}  // namespace drivers

#endif /* EEPROM_I2C_H */
