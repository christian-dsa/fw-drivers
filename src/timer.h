/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TIMEOUT_H
#define TIMEOUT_H

#include "tick.h"
#include <limits>

namespace drivers {

class Timer {

public:
    explicit Timer(Tick& instance)
    : tick(&instance) {}

    void start_countdown(std::uint32_t countdown_ms) {
        // Start value
        tick_start = tick->get_tick_value();

        // Number of tick to reach timeout
        tick_timeout = countdown_ms / tick->get_resolution_ms();

        // 0 not allowed
        if (tick_timeout == 0) {
            ++tick_timeout;
        }
    }

    [[nodiscard]] bool is_countdown_over() const {
        return ((tick->get_tick_value() - tick_start) >= tick_timeout);
    }

    // Blocking delay
    void delay_ms(std::uint32_t delay_ms) const {

        // Save tick value at beginning
        const std::uint32_t start     = tick->get_tick_value();
        const std::uint32_t tick_wait = (delay_ms / tick->get_resolution_ms());

        std::uint32_t tick_value = start;
        while ((tick_value - start) < tick_wait) {
            tick_value = tick->get_tick_value();
        }
    }

private:
    Tick*         tick;
    std::uint32_t tick_start{0};
    std::uint32_t tick_timeout{0};
};
}  // namespace drivers

#endif /* TIMEOUT_H */
