/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LED_H
#define LED_H

#include "gpio.h"
#include "pwm.h"
#include <cmath>
#include <cstdint>
#include <limits>
#include <type_traits>

namespace drivers {

class Led {
public:
    enum class Active { low, high };
    enum class State { off, on };

    Led(Gpio& gpio, Active active)
    : gpio(&gpio),
      active(active) {}

    void on() {
        if (active == Active::low) {
            gpio->reset_pin();
        } else {
            gpio->set_pin();
        }
    }

    void off() {
        if (active == Active::low) {
            gpio->set_pin();
        } else {
            gpio->reset_pin();
        }
    }

    State read() {
        const Gpio::State pin = gpio->read();

        State state = (pin == Gpio::State::set) ? State::on : State::off;
        if (active == Active::low) {
            state = (state == State::on) ? State::off : State::on;
        }
        return state;
    }

private:
    Gpio*  gpio;
    Active active;
};

class Led_pwm {
public:
    explicit Led_pwm(Pwm& pwm)
    : pwm(&pwm),
      pulse_width_on(pulse_width_off) {}

    void open() {
        off();
        pwm->open();
    }

    void close() {
        off();
        pwm->close();
    }

    void on() {
        set_pwm_pulse_width();
    }

    // Note: The PWM duty cycle = intensity / datatype max value. For float, datatype max is 1.0.
    // Ex: intensity is 127, datatype is uint8_t. PWM = 127 / 255. PWM = ~50%.
    // Risk of overflow when using integer.
    // Architecture size > (datatype size in bit + pwm period size in bit).
    template<typename datatype>
    void on(datatype intensity) {
        set_intensity(intensity);
        set_pwm_pulse_width();
    }

    void off() {
        set_pwm_off();
    }

    static float gamma_correction(float intensity, float gamma) {
        const float tmp = intensity;
        return powf(tmp, gamma);
    }

private:
    void set_pwm_off() {
        pwm->set_pulse_width(pulse_width_off);
    }

    void set_pwm_pulse_width() {
        pwm->set_pulse_width(pulse_width_on);
    }

    template<typename datatype>
    void set_intensity(datatype intensity) {
        if constexpr (std::is_integral_v<datatype> && std::is_unsigned_v<datatype>) {
            pulse_width_on = pwm->get_period() * intensity / std::numeric_limits<datatype>::max();
        } else {
            if (intensity >= max_intensity_f) {
                pulse_width_on = pwm->get_period() + 1;
            } else {
                pulse_width_on = static_cast<float>(pwm->get_period()) * intensity;
            }
        }
    }

    // Variables
    Pwm*          pwm;
    std::uint32_t pulse_width_on;

    // Constants
    static constexpr std::uint32_t pulse_width_off = 0;
    static constexpr float         max_intensity_f = 1.0;
};
}  // namespace drivers

#endif /* LED_H */
