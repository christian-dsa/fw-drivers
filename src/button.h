/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BUTTON_H
#define BUTTON_H

#include "gpio.h"
#include "timer.h"

namespace drivers {

class Button {
public:
    enum class State { idle, pressed, released };

    enum class Active { low, high };

    Button(Gpio& gpio, Active active)
    : gpio(&gpio),
      active(active) {}

    State read() {
        const Gpio::State pin   = gpio->read();
        State             state = State::idle;

        if (is_pressed(pin)) {
            state = State::pressed;
        } else if (last_state == State::pressed) {
            state = State::released;
        }
        last_state = state;
        return state;
    }

private:
    bool is_pressed(Gpio::State pin) {
        bool pressed = false;

        if (((active == Active::low) && (pin == Gpio::State::reset)) ||
            ((active == Active::high) && (pin == Gpio::State::set))) {
            pressed = true;
        }

        return pressed;
    }

    Active active;
    State  last_state{State::idle};
    Gpio*  gpio;
};
}  // namespace drivers

#endif /* BUTTON_H */
