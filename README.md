# FW-Drivers

**Develop**  
[![pipeline status](https://gitlab.com/christian-dsa/fw-drivers/badges/develop/pipeline.svg)](https://gitlab.com/christian-dsa/fw-drivers/-/commits/develop)
[![coverage report](https://gitlab.com/christian-dsa/fw-drivers/badges/develop/coverage.svg)](https://gitlab.com/christian-dsa/fw-drivers/-/commits/develop)

## Library option (compile definitions)

### LED_DISABLE_GAMMA_CORRECTION

Even if gamma correction is enabled, no correction is applied. This option allow to save 
multiples kB of flash.


## Test option

It is possible to test the EEPROM I2C driver with real hardware using the cmake option `-DHARDWARE_EEPROM_I2C=on`.
The PC MUST be connected to a FT4222H FTDI and any I2C eeprom. The page size used in test is 8 byte.
